﻿using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osfur_RoleplayEvents.Model
{
    public class RobberModel
    {
        public CSteamID Robber { get; set; }

        public List<CSteamID> VictimList { get; set; }

        public ushort Timeout { get; set; }
    }
}