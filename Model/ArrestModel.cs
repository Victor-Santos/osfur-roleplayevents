﻿using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osfur_RoleplayEvents.Model
{
    public class ArrestModel
    {
        public CSteamID Officer { get; set; }

        public List<CSteamID> ArrestedPlayerList { get; set; }

        public ushort Timeout { get; set; }
    }
}