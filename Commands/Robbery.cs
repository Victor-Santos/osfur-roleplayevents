﻿using Osfur_RoleplayEvents.Model;
using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class Robbery : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Rob a player or multiple players.";

        public string Name => "rob";

        public List<string> Permissions => new List<string>();

        public string Syntax => "/rob <first-player> <second-player> <third-player> <fourth-player>";

        public static RobberModel ActiveRobber = new RobberModel()
        {
            Robber = new CSteamID(),
            VictimList = new List<CSteamID>(),
            Timeout = 0
        };

        public static Dictionary<CSteamID, ushort> RecentlyRobber = new Dictionary<CSteamID, ushort>();

        public static Dictionary<CSteamID, ushort> RecentlyRobbed = new Dictionary<CSteamID, ushort>();

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            //check command syntax
            if (Command.Length < 1 || Command.Length > 4)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_syntax"), Color.red);
                return;
            }

            //check if a robbery is happening
            if (ActiveRobber.Robber.IsValid())
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_already_happening"), Color.red);
                return;
            }

            UnturnedPlayer PlayerCaller = (UnturnedPlayer)Caller;

            //check if the player recently robbed someone
            if (RecentlyRobber.ContainsKey(PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_recently_robber", RecentlyRobber[PlayerCaller.CSteamID]), Color.red);
                return;
            }

            List<UnturnedPlayer> VictimList = new List<UnturnedPlayer>();

            foreach (var Player in Command)
            {
                VictimList.Add(UnturnedPlayer.FromName(Player));
            }

            foreach (var Victim in VictimList)
            {
                //check if a player exists
                if (Victim == null)
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_players_not_found"), Color.red);
                    return;
                }

                //check if it is targeting yourself
                if (PlayerCaller.CSteamID == Victim.CSteamID)
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("common_target_yourself"), Color.red);
                    return;
                }

                //check if both players are near each other
                if (Vector3.Distance(PlayerCaller.Position, Victim.Position) > Main.Instance.Configuration.Instance.RobRange)
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_players_not_in_range"), Color.red);
                    return;
                }

                //check if the victim was recently robbed
                if (RecentlyRobbed.ContainsKey(Victim.CSteamID))
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_recently_robbed"), Color.red);
                    return;
                }
            }

            UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_rob_public_message"));

            Rocket.Core.Logging.Logger.Log("Robber Name: " + PlayerCaller.CharacterName);
            Rocket.Core.Logging.Logger.Log("Robber SteamID: " + PlayerCaller.Id);

            ActiveRobber.Robber = PlayerCaller.CSteamID;

            ActiveRobber.VictimList = new List<CSteamID>();

            foreach (var Victim in VictimList)
            {
                UnturnedChat.Say(Victim, Main.Instance.Translations.Instance.Translate("command_rob_private_message", PlayerCaller.CharacterName));

                ActiveRobber.VictimList.Add(Victim.CSteamID);

                Rocket.Core.Logging.Logger.Log(PlayerCaller.CharacterName + " is robbing " + Victim.CharacterName);
                Rocket.Core.Logging.Logger.Log("Victim SteamID: " + Victim.Id);
            }

            //starts timeout
            ActiveRobber.Timeout = Main.Instance.Configuration.Instance.RobTimeout;
        }

        public static void StartCooldown(bool DisplayMessage = true)
        {
            if (DisplayMessage)
            {
                UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_robend_ended"));
            }

            RecentlyRobber.Add(ActiveRobber.Robber, Main.Instance.Configuration.Instance.RobberCooldown); //cooldown robber

            foreach (var Victim in ActiveRobber.VictimList)
            {
                RecentlyRobbed.Add(Victim, Main.Instance.Configuration.Instance.RobbedCooldown); //cooldown each victim
            }

            //add cooldown for helper
            foreach (var Helper in RobAid.ActiveHelpRobbery)
            {
                RecentlyRobber.Add(Helper, Main.Instance.Configuration.Instance.RobberCooldown);
            }

            //add cooldown for interveners
            foreach (var Intervenee in RobIntervene.RobberyIntervenees)
            {
                RobIntervene.RecentlyIntervened.Add(Intervenee, Main.Instance.Configuration.Instance.InterveneCooldown);
            }

            ActiveRobber.Robber = new CSteamID();
            ActiveRobber.VictimList = new List<CSteamID>();
            ActiveRobber.Timeout = 0;

            RobAid.ActiveHelpRobbery.Clear();
            RobIntervene.RobberyIntervenees.Clear();
        }
    }
}