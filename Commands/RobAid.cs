﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class RobAid : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Aids an active robbery.";

        public string Name => "robaid";

        public List<string> Permissions => new List<string>();

        public string Syntax => "/robaid";

        public static List<CSteamID> ActiveHelpRobbery = new List<CSteamID>();

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            //check command syntax
            if (Command.Length != 0)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_robaid_syntax"), Color.red);
                return;
            }

            UnturnedPlayer PlayerCaller = (UnturnedPlayer)Caller;

            //check if a robbery is not happening
            if (!Robbery.ActiveRobber.Robber.IsValid())
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("common_no_robbery_happening"), Color.red);
                return;
            }

            //check if player is on a cooldown
            if (Robbery.RecentlyRobber.ContainsKey(PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_recently_robber", Robbery.RecentlyRobber[PlayerCaller.CSteamID]), Color.red);
                return;
            }

            //check if player is already helping
            if (ActiveHelpRobbery.Contains(PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_robaid_already_helping"), Color.red);
                return;
            }

            //check if robbing itself
            if (PlayerCaller.CSteamID == Robbery.ActiveRobber.Robber)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("common_target_yourself"), Color.red);
                return;
            }

            var Robber = UnturnedPlayer.FromCSteamID(Robbery.ActiveRobber.Robber);

            //check if player is close to the robber
            if (Vector3.Distance(PlayerCaller.Position, Robber.Position) > Main.Instance.Configuration.Instance.RobAidRange)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_player_not_in_range"), Color.red);
                return;
            }

            //public message robaid
            UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_robaid_joined_robbery"));

            //private message
            UnturnedChat.Say(PlayerCaller, Main.Instance.Translations.Instance.Translate("command_robaid_private_message", UnturnedPlayer.FromCSteamID(Robbery.ActiveRobber.Robber).CharacterName));

            //send message to the robber saying who joined the robbery
            UnturnedChat.Say(Robbery.ActiveRobber.Robber, Main.Instance.Translations.Instance.Translate("command_robaid_person_joined", PlayerCaller.CharacterName));

            //send a private message to the victims saying who joined the robbery
            foreach (var Victim in Robbery.ActiveRobber.VictimList)
            {
                UnturnedChat.Say(Victim, Main.Instance.Translations.Instance.Translate("command_robaid_person_joined", PlayerCaller.CharacterName));
            }

            //send a private message to the helper
            foreach (var Helper in ActiveHelpRobbery)
            {
                UnturnedChat.Say(Helper, Main.Instance.Translations.Instance.Translate("command_robaid_person_joined", PlayerCaller.CharacterName));
            }

            Rocket.Core.Logging.Logger.Log(PlayerCaller.CharacterName + " joined the robbery.");

            //add this player as a helper
            ActiveHelpRobbery.Add(PlayerCaller.CSteamID);
        }
    }
}