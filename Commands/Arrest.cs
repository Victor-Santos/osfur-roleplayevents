﻿using Osfur_RoleplayEvents.Model;
using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class Arrest : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Arrests a player.";

        public string Name => "arrest";

        public List<string> Permissions => new List<string>();

        public string Syntax => "/arrest <first-player> <second-player> <third-player> <fourth-player>";

        public static List<ArrestModel> PlayerArrestList = new List<ArrestModel>();

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            if (Command.Length < 1 && Command.Length > 4)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_arrest_syntax"), Color.red);
                return;
            }

            UnturnedPlayer PlayerCaller = (UnturnedPlayer)Caller;

            if (PlayerArrestList.Any(x => x.Officer == PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_arrest_already_arresting"), Color.red);
                return;
            }

            List<UnturnedPlayer> ArrestedPlayerList = new List<UnturnedPlayer>();

            foreach (var Player in Command)
            {
                ArrestedPlayerList.Add(UnturnedPlayer.FromName(Player));
            }

            foreach (var Player in ArrestedPlayerList)
            {
                //check if a player exists
                if (Player == null)
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_players_not_found"), Color.red);
                    return;
                }

                //check if it is targeting yourself
                if (PlayerCaller.CSteamID == Player.CSteamID)
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("common_target_yourself"), Color.red);
                    return;
                }
            }

            ArrestModel ArrestModel = new ArrestModel()
            {
                Officer = PlayerCaller.CSteamID,
                ArrestedPlayerList = ArrestedPlayerList.Select(x => x.CSteamID).ToList(),
                Timeout = Main.Instance.Configuration.Instance.ArrestTimeout
            };

            PlayerArrestList.Add(ArrestModel);

            StringBuilder ArrestedPlayersMessage = new StringBuilder();

            foreach (var Player in ArrestedPlayerList)
            {
                ArrestedPlayersMessage.Append(Player.CharacterName + ", ");
            }

            ArrestedPlayersMessage.Length -= 2;

            UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_arrest_arresting", ArrestedPlayersMessage.ToString()));

            foreach (var Player in ArrestedPlayerList)
            {
                UnturnedChat.Say(Player, Main.Instance.Translations.Instance.Translate("command_arrest_geting_arrested", PlayerCaller.CharacterName));
                Rocket.Core.Logging.Logger.Log(PlayerCaller.CharacterName + " is arresting " + Player.CharacterName + ".");
            }

            UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_arrest_global", ArrestedPlayersMessage.ToString()));
            return;
        }
    }
}