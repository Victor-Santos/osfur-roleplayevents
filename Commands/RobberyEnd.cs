﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class RobberyEnd : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Ends a robbery.";

        public string Name => "robend";

        public List<string> Permissions => new List<string>();

        public string Syntax => "/robend";

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            //command syntax
            if (Command.Length != 0)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_robend_syntax"), Color.red);
                return;
            }

            UnturnedPlayer PlayerCaller = (UnturnedPlayer)Caller;

            //check if player is robbing somoene
            if (Robbery.ActiveRobber.Robber != PlayerCaller.CSteamID)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_robend_not_robbing"), Color.red);
                return;
            }

            //starts all cooldowns and send the public message
            Robbery.StartCooldown();

            //send a message to all victims 
            foreach (var Victim in Robbery.ActiveRobber.VictimList)
            {
                UnturnedChat.Say(Victim, Main.Instance.Translations.Instance.Translate("command_robend_private_message", PlayerCaller.CharacterName));
            }

            Rocket.Core.Logging.Logger.Log(PlayerCaller.CharacterName + " ended his robbery.");
        }
    }
}