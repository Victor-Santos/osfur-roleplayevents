﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class RobIntervene : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Arrests a player.";

        public string Name => "robintervene";

        public List<string> Permissions => new List<string>();

        public string Syntax => "/robintervene";

        public static List<CSteamID> RobberyIntervenees = new List<CSteamID>();

        public static Dictionary<CSteamID, ushort> RecentlyIntervened = new Dictionary<CSteamID, ushort>();

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            if (Command.Length != 0)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_intervene_syntax"), Color.red);
                return;
            }

            //check if robbery is not happening
            if (!Robbery.ActiveRobber.Robber.IsValid())
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("common_no_robbery_happening"), Color.red);
                return;
            }

            UnturnedPlayer PlayerCaller = (UnturnedPlayer)Caller;

            //check if you are already intervening
            if (RobberyIntervenees.Contains(PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_intervene_already_intervening"), Color.red);
                return;
            }

            //check if you are on the cooldown
            if (RecentlyIntervened.ContainsKey(PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_intervene_recently_intervened", RecentlyIntervened[PlayerCaller.CSteamID]), Color.red);
                return;
            }

            //check if the intervener are not involved in the robbery
            if (CheckInvolved(PlayerCaller))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_intervene_involved_robbery"), Color.red);
                return;
            }

            //check if the intervener are close to anyone involved in the robbery
            if (!CheckNear(PlayerCaller))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_intervene_not_close"), Color.red);
                return;
            }

            RobberyIntervenees.Add(PlayerCaller.CSteamID);

            //message robber
            UnturnedChat.Say(Robbery.ActiveRobber.Robber, Main.Instance.Translations.Instance.Translate("command_rob_intervene_intervened", PlayerCaller.CharacterName));

            //message victim
            foreach (var Victim in Robbery.ActiveRobber.VictimList)
            {
                UnturnedChat.Say(Victim, Main.Instance.Translations.Instance.Translate("command_rob_intervene_intervened", PlayerCaller.CharacterName));
            }

            //message helpers
            foreach (var Helper in RobAid.ActiveHelpRobbery)
            {
                UnturnedChat.Say(Helper, Main.Instance.Translations.Instance.Translate("command_rob_intervene_intervened", PlayerCaller.CharacterName));
            }

            //message public about the intervening
            UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_rob_intervene_public_message"));

            //message private to intervenee
            UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_rob_intervene_private_message"));
            return;
        }

        private bool CheckInvolved(UnturnedPlayer PlayerCaller)
        {
            if (Robbery.ActiveRobber.Robber == PlayerCaller.CSteamID ||
               Robbery.ActiveRobber.VictimList.Contains(PlayerCaller.CSteamID) ||
               RobAid.ActiveHelpRobbery.Contains(PlayerCaller.CSteamID))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckNear(UnturnedPlayer PlayerCaller)
        {
            float Range = Main.Instance.Configuration.Instance.RobInterveneRange;

            //check if the intervener is close to the robber
            if (Vector3.Distance(PlayerCaller.Position, UnturnedPlayer.FromCSteamID(Robbery.ActiveRobber.Robber).Position) < Range)
            {
                return true;
            }

            //check if the intervener is close to any of the victims
            foreach (var Victim in Robbery.ActiveRobber.VictimList)
            {
                if (Vector3.Distance(PlayerCaller.Position, UnturnedPlayer.FromCSteamID(Victim).Position) < Range)
                {
                    return true;
                }
            }

            //check if the intervener is close to any of the helper
            foreach (var Helper in RobAid.ActiveHelpRobbery)
            {
                if (Vector3.Distance(PlayerCaller.Position, UnturnedPlayer.FromCSteamID(Helper).Position) < Range)
                {
                    return true;
                }
            }

            return false;
        }
    }
}