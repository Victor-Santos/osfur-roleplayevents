﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class ArrestEnd : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Arrests a player.";

        public string Name => "arrestend";

        public List<string> Permissions => new List<string>();

        public string Syntax => "<player>";

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            if (Command.Length != 0)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_arrest_end_syntax"), Color.red);
                return;
            }

            UnturnedPlayer PlayerCaller = (UnturnedPlayer)Caller;

            if (!Arrest.PlayerArrestList.Any(x => x.Officer == PlayerCaller.CSteamID))
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_arrest_end_not_arresting"), Color.red);
                return;
            }

            UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_arrest_end_global"));

            Arrest.PlayerArrestList.Remove(Arrest.PlayerArrestList.FirstOrDefault(x => x.Officer == PlayerCaller.CSteamID));
            return;
        }
    }
}