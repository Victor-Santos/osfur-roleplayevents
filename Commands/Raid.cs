﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class Raid : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Arrests a player.";

        public string Name => "raid";

        public List<string> Permissions => new List<string>();

        public string Syntax => "/raid <location>";

        public static ushort TimeLeft = 0;

        public static string Location = "";

        public static ushort RaidCooldown = 0;

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            if (Command.Length < 1)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_raid_syntax"), Color.red);
                return;
            }

            if (TimeLeft > 0)
            {
                UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_raid_already_happening"), Color.red);
                return;
            }

            if (Main.Instance.Configuration.Instance.RaidCooldown != 0)
            {
                if (RaidCooldown > 0)
                {
                    UnturnedChat.Say(Caller, Main.Instance.Translations.Instance.Translate("command_raid_cooldown"), Color.red);
                    return;
                }

                RaidCooldown = Main.Instance.Configuration.Instance.RaidCooldown;
            }

            StringBuilder LocationSB = new StringBuilder();

            foreach (var Parameter in Command)
            {
                LocationSB.Append(Parameter + " ");
            }

            LocationSB.Length--;

            Location = LocationSB.ToString();
            TimeLeft = Main.Instance.Configuration.Instance.PoliceRaidCountdown;

            UnturnedChat.Say(Main.Instance.Translations.Instance.Translate("command_raid_start", Location, Main.Instance.Configuration.Instance.PoliceRaidCountdown, Main.Instance.Configuration.Instance.PoliceRaidCountdown == 1 ? "minute" : "minutes"));
            return;
        }
    }
}