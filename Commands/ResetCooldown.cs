﻿using Rocket.API;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Osfur_RoleplayEvents.Commands
{
    public class ResetCooldown : IRocketCommand
    {
        public List<string> Aliases => new List<string>();

        public AllowedCaller AllowedCaller => AllowedCaller.Player;

        public string Help => "Ends a robbery.";

        public string Name => "reset";

        public List<string> Permissions => new List<string>();

        public string Syntax => "<player>";

        public void Execute(IRocketPlayer Caller, string[] Command)
        {
            Arrest.PlayerArrestList = new List<Model.ArrestModel>();
            Raid.TimeLeft = 0;
            Raid.Location = "";            
            Raid.RaidCooldown = 0;
            RobAid.ActiveHelpRobbery = new List<CSteamID>();
            Robbery.ActiveRobber = new Model.RobberModel();
            Robbery.RecentlyRobbed.Clear();
            Robbery.RecentlyRobber.Clear();
            RobIntervene.RobberyIntervenees = new List<CSteamID>();
            RobIntervene.RecentlyIntervened = new Dictionary<CSteamID, ushort>();

            Rocket.Core.Logging.Logger.Log("Osfur Roleplay Events was reloaded.");
        }
    }
}