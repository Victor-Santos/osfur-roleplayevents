﻿using Rocket.API;
using System;

namespace Osfur_RoleplayEvents
{
    public class Configuration : IRocketPluginConfiguration
    {
        public float RobRange;
        public float RobAidRange;
        public float RobInterveneRange;

        public ushort RobTimeout;
        public ushort ArrestTimeout;

        public ushort RobberCooldown;
        public ushort RobbedCooldown;
        public ushort InterveneCooldown;
        public ushort RaidCooldown;

        public ushort PoliceRaidCountdown;

        public void LoadDefaults()
        {
            RobRange = 5;
            RobAidRange = 10;
            RobInterveneRange = 5;

            RobTimeout = 5;
            ArrestTimeout = 10;

            RobberCooldown = 60;
            RobbedCooldown = 15;
            InterveneCooldown = 30;
            RaidCooldown = 0;

            PoliceRaidCountdown = 5;
        }
    }
}