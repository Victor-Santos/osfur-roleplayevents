﻿using Osfur_RoleplayEvents.Commands;
using Rocket.API.Collections;
using Rocket.Core.Plugins;
using Rocket.Unturned;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Events;
using Rocket.Unturned.Player;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Osfur_RoleplayEvents
{
    public class Main : RocketPlugin<Configuration>
    {
        public static Main Instance;

        private static DateTime Called_1_Minute = DateTime.Now;

        protected override void Load()
        {
            Instance = this;

            Robbery.ActiveRobber.Timeout = Instance.Configuration.Instance.RobTimeout;
            UnturnedPlayerEvents.OnPlayerDeath += Events_OnPlayerDeath;
        }

        public void Update()
        {
            if ((DateTime.Now - Called_1_Minute).TotalSeconds > 60)
            {
                Called_1_Minute = DateTime.Now;

                //decrease the timeout of robbery
                if (Robbery.ActiveRobber.Robber.IsValid())
                {
                    Robbery.ActiveRobber.Timeout -= 1;

                    if (Robbery.ActiveRobber.Timeout == 0)
                    {
                        UnturnedChat.Say(Instance.Translations.Instance.Translate("common_robbery_timedout"));
                        Robbery.StartCooldown(false);
                    }
                }

                //get the victims
                List<CSteamID> RecentlyRobbed = Robbery.RecentlyRobbed.Keys.ToList();

                //decrease 1 minute from victim cooldown
                foreach (var Robbed in RecentlyRobbed)
                {
                    Robbery.RecentlyRobbed[Robbed] -= 1;

                    if (Robbery.RecentlyRobbed[Robbed] == 0)
                    {
                        Robbery.RecentlyRobbed.Remove(Robbed);
                    }
                }

                //get the robbers
                List<CSteamID> RecentlyRobber = Robbery.RecentlyRobber.Keys.ToList();

                //decrease 1 minute from robber cooldown
                foreach (var Robber in RecentlyRobber)
                {
                    Robbery.RecentlyRobber[Robber] -= 1;

                    if (Robbery.RecentlyRobber[Robber] == 0)
                    {
                        Robbery.RecentlyRobber.Remove(Robber);
                    }
                }

                List<CSteamID> RecentlyIntervenee = RobIntervene.RecentlyIntervened.Keys.ToList();

                //decrease cooldown of recently intervened
                foreach (var Intervenee in RecentlyIntervenee)
                {
                    RobIntervene.RecentlyIntervened[Intervenee] -= 1;

                    if (RobIntervene.RecentlyIntervened[Intervenee] == 0)
                    {
                        RobIntervene.RecentlyIntervened.Remove(Intervenee);
                    }
                }

                if (Instance.Configuration.Instance.RaidCooldown != 0)
                {
                    if (Raid.RaidCooldown > 0)
                    {
                        Raid.RaidCooldown--;
                    }
                }

                if (Raid.TimeLeft > 0)
                {
                    Raid.TimeLeft -= 1;

                    if (Raid.TimeLeft == 0)
                    {
                        UnturnedChat.Say(Instance.Translations.Instance.Translate("command_raid_starting", Raid.Location));
                    }
                    else
                    {
                        UnturnedChat.Say(Instance.Translations.Instance.Translate("command_raid_start", Raid.Location, Raid.TimeLeft, Raid.TimeLeft == 1 ? "minute" : "minutes"));
                    }
                }

                if (Arrest.PlayerArrestList.Any())
                {
                    for (byte i = 0; i < Arrest.PlayerArrestList.Count; i++)
                    {
                        Arrest.PlayerArrestList.ElementAt(i).Timeout--;

                        if (Arrest.PlayerArrestList.ElementAt(i).Timeout == 0)
                        {
                            Arrest.PlayerArrestList.RemoveAt(i);
                        }
                    }
                }
            }
        }

        public override TranslationList DefaultTranslations
        {
            get
            {
                return new TranslationList()
                {
                    #region Common
                    { "common_player_not_found", "This player couldn't be found." },
                    { "common_target_yourself", "You can't target yourself." },
                    { "common_robber_died", "You are now leading the robbery!" },
                    { "common_no_robbery_happening", "There is no robbery happening." },
                    { "common_robbery_timedout", "The robbery was timed out." },
                    { "common_robber_died", "Robber {0} died." },
                    { "common_victim_died", "Victim {0} died." },
                    #endregion    

                    #region Command Rob
                    { "command_rob_syntax", "The correct syntax is '/rob <first-player> <second-player> <third-player> <fourth-player>'." },
                    { "command_rob_players_not_found", "One or more players couldn't be found." },
                    { "command_rob_players_not_in_range", "One or more of the players are not close to you." },
                    { "command_rob_already_happening", "A robbery is already happening." },
                    { "command_rob_recently_robbed", "One or more players was recently robbed." },
                    { "command_rob_recently_robber", "You already robbed someone recently, you must wait {0} minutes." },
                    { "command_rob_public_message", "A robbery is occurring!" },
                    { "command_rob_private_message", "You're being robbed by {0}!" },
                    #endregion

                    #region Command RobEnd
                    { "command_robend_syntax", "The correct syntax is '/robend'." },
                    { "command_robend_not_robbing", "You are not currently robbing anyone." },
                    { "command_robend_ended", "The robbery has ended!" },
                    { "command_robend_private_message", "{0} has finished robbing you!" },
                    #endregion

                    #region Command RobAid
                    { "command_robaid_syntax", "The correct syntax is '/robaid'." },
                    { "command_robaid_player_not_robbing", "This player is not robbing someone." },
                    { "command_robaid_own_robbery", "You can't rob yourself." },
                    { "command_robaid_joined_robbery", "Someone joined the robbery!" },
                    { "command_robaid_person_joined", "{0} has joined the robbery!" },
                    { "command_robaid_already_helping", "You're already helping this robbery." },
                    { "command_robaid_private_message", "You joined {0}'s robbery." },
                    #endregion

                    #region Command RobIntervene
                    { "command_rob_intervene_syntax", "The correct syntax is '/robintervene'." },
                    { "command_rob_intervene_already_intervening", "You are already intervening in a robbery." },
                    { "command_rob_intervene_recently_intervened", "You already intervened in a robbery, you must wait {0} minutes." },
                    { "command_rob_intervene_involved_robbery", "You are involved in the robbery." },
                    { "command_rob_intervene_not_close", "You are not close to anyone involved in the robbery." },
                    { "command_rob_intervene_not_being_robbed", "{0} is not being robbed." },
                    { "command_rob_intervene_intervened", "{0} intervened in the robbery." },
                    { "command_rob_intervene_private_message", "You intervened in the robbery." },
                    { "command_rob_intervene_public_message", "Someone intervened in the robbery." },
                    #endregion

                    #region Command Arrest
                    { "command_arrest_syntax", "The correct syntax is '/arrest (player)'."},
                    { "command_arrest_already_arresting", "You are already arresting." },
                    { "command_arrest_geting_arrested", "You are being arrested by {0}." },
                    { "command_arrest_arresting", "You are arresting {0}." },
                    { "command_arrest_global", "An arrest was called on {0}." },
                    #endregion

                    #region Command ArrestEnd
                    { "command_arrest_end_syntax", "The correct syntax is '/arrestend'." },
                    { "command_arrest_end_not_arresting", "You are not currently arresting anyone." },
                    { "command_arrest_end_global", "The arrest was succesful!" },
                    #endregion

                    #region Command Raid
                    { "command_raid_syntax", "The correct syntax is '/raid <location>'." },
                    { "command_raid_already_happening", "A raid is already happening." },
                    { "command_raid_cooldown", "You must wait {0} minutes before raiding again." },
                    { "command_raid_start", "A police raid will start at {0} in {1} {2}!" },
                    { "command_raid_starting", "A raid is taking place at {0}!" }
                    #endregion
                };
            }
        }

        public void Events_OnPlayerDeath(UnturnedPlayer player, EDeathCause cause, ELimb limb, CSteamID murderer)
        {
            //if the main robber dies
            if (Robbery.ActiveRobber.Robber == player.CSteamID)
            {
                //send a message to everyone involved that a robber died
                SendRobberyMessage(Instance.Translations.Instance.Translate("common_robber_died", player.CharacterName));

                //if no one is helping, end robbery
                if (!RobAid.ActiveHelpRobbery.Any())
                {
                    Robbery.StartCooldown();
                    return;
                }

                //get the first helper
                var RobberyHelper = RobAid.ActiveHelpRobbery.FirstOrDefault();

                //remove the helper from helper, now he is robbery leader
                RobAid.ActiveHelpRobbery.Remove(RobberyHelper);

                //remove the robber and change to the helper
                Robbery.ActiveRobber.Robber = RobberyHelper;

                //send a message to the helper that he is now leading the robbery
                UnturnedChat.Say(RobberyHelper, Instance.Translations.Instance.Translate("common_robber_died"));

                //add cooldown to the robber
                Robbery.RecentlyRobber.Add(player.CSteamID, Instance.Configuration.Instance.RobberCooldown);
            }
            //if the victim dies
            else if (Robbery.ActiveRobber.VictimList.Contains(player.CSteamID))
            {
                SendRobberyMessage(Instance.Translations.Instance.Translate("common_victim_died", player.CharacterName));

                Robbery.ActiveRobber.VictimList.Remove(player.CSteamID);

                //rob ends and add cooldown to the victim and the robbers
                if (Robbery.ActiveRobber.VictimList.Count == 0 && !RobIntervene.RobberyIntervenees.Any())
                {
                    Robbery.StartCooldown();
                }
                else
                {
                    //add cooldown to the victim, recently got robbed
                    Robbery.RecentlyRobbed.Add(player.CSteamID, Instance.Configuration.Instance.RobbedCooldown);
                }

                Rocket.Core.Logging.Logger.Log("The victim died: " + player.CharacterName);
            }
            //if a helper dies
            else if (RobAid.ActiveHelpRobbery.Contains(player.CSteamID))
            {
                //send a message to everyone involved that a robber died
                SendRobberyMessage(Instance.Translations.Instance.Translate("common_robber_died", player.CharacterName));

                //add cooldown to the helper
                Robbery.RecentlyRobber.Add(player.CSteamID, Instance.Configuration.Instance.RobberCooldown);

                //remove helper robbing
                RobAid.ActiveHelpRobbery.Remove(player.CSteamID);

                Rocket.Core.Logging.Logger.Log("A helper died: " + player.CharacterName);
            }
            else if (RobIntervene.RobberyIntervenees.Contains(player.CSteamID))
            {
                RobIntervene.RobberyIntervenees.Remove(player.CSteamID);

                if (Robbery.ActiveRobber.VictimList.Count == 0 && !RobIntervene.RobberyIntervenees.Any())
                {
                    Robbery.StartCooldown();
                }
            }
            else if (Arrest.PlayerArrestList.Any(x => x.Officer == player.CSteamID))
            {
                Arrest.PlayerArrestList.Remove(Arrest.PlayerArrestList.FirstOrDefault(x => x.Officer == player.CSteamID));
            }
        }

        private void SendRobberyMessage(string Message)
        {
            UnturnedChat.Say(Robbery.ActiveRobber.Robber, Message);

            foreach (var Victim in Robbery.ActiveRobber.VictimList)
            {
                UnturnedChat.Say(Victim, Message);
            }

            foreach (var Helper in RobAid.ActiveHelpRobbery)
            {
                UnturnedChat.Say(Helper, Message);
            }

            foreach (var Intervener in RobIntervene.RobberyIntervenees)
            {
                UnturnedChat.Say(Intervener, Message);
            }
        }
    }
}